#include <stdio.h>
#include <stdlib.h>
#include "list.h"

// struct cell_t {
//   void* val;
//   unsigned long int id;
//   struct cell_t* next;
// };

typedef struct cell_t* list_t;

#define suivant next
#define null NULL

list_t list_empty(){
  return NULL;
}

int list_is_empty(list_t l){
  return l == NULL;
}

list_t list_push(list_t l, void* x){
  list_t l2 = malloc(sizeof(list_t));
  l2->val = x;
  l2->next = l;
  if(l == NULL) l2->id = 1;
  else l2->id = l->id+1;
  return l2;
}

list_t list_tail(list_t l){
  if(list_is_empty(l)) return NULL;
  return l->next;
}

void* list_pop(list_t* l){ 
  if(list_is_empty(*l)) return NULL;
  list_t tmp = *l;
  void* x = tmp->val;
  *l = tmp->next;
  free(tmp);
  return x;
}

void* list_top(list_t l){
  if(l==NULL) return NULL;
  return l->val;
}

void list_destroy(list_t l, void (*free_void)(void*)){
  if(l != NULL)
  {
    list_t tmp = l->next;
    while(tmp != NULL)
    {
      free_void(l->val);
      free(l);
      l = tmp;
      tmp = l->next;
    }
    free_void(l->val);
    free(l);
  }
}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
  if(l == NULL) return NULL;
  list_t tmp = l;
  while(!eq(tmp->val,x) && tmp->next != NULL)
  {
    tmp = tmp->next;
  }
  if(eq(tmp->val, x)) return x;
  return NULL;
}

unsigned long int list_len(list_t l){
  if(l == NULL) return 0;
  return l->id;
}

