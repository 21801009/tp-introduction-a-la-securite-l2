#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "bignum.h"
#include "list.h"

unsigned long int gcd(unsigned long int a, unsigned long int b){
  unsigned long int old_r, r = 1;
  if(a < b)
    return gcd(b,a);

  while(r != 0){
    old_r = r;
    r = a % b;
    a = b;
    b = r;
  }

  return old_r;
}

int f(x) {
  return x*x+1;
}

int has_cycle(list_t l, int n, unsigned long int *d) {
  srand(time(NULL));
  l = list_is_empty(l);
  int x = rand() % n;
  l = list_push(l, x);
  x = f(x) % n;
  int xk;
  l = list_push(l, x);  
  while((*d=gcd(abs(x-f(x)), n)) != 1) {
    xk = list_top(l);
    l = list_push(l, xk);
    l = list_tail(l);
  }
  return 0;
}

// rho_pollard

int main() {
  // En utilisant l'algorithme rho de Pollard, factorisez les entiers suivants

  unsigned long int n1 = 17 * 113;
  unsigned long int n2 = 239 * 431;
  unsigned long int n3 = 3469 * 4363;
  unsigned long int n4 = 15241 * 18119;
  unsigned long int n5 = 366127l * 416797l;
  unsigned long int n6 = 15651941l * 15485863l;

  bignum_t n7, n8;
 
  n7 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(127)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(61)),
                             bignum_from_int(1)));

  n8 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(607)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(2203)),
                             bignum_from_int(1)));


  
  printf("PGCD(42,24) = %lu\n", gcd(42,24));
  printf("PGCD(42,24) = %s\n", bignum_to_str(bignum_gcd(bignum_from_int(42),bignum_from_int(24))));
  
  return 0;
}
